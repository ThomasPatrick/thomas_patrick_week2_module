package com.example.myapplication3;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //Declare the objects
    private TextView txtView1;
    private TextView txtView2;
    private TextView txtView3;
    private EditText aEditText;
    private Button btnSubmit;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newlayout);

        //Locate property & assign a value
        txtView1 = (TextView) findViewById(R.id.txtView1);
        txtView2 = (TextView) findViewById(R.id.txtView2);
        txtView3 = (TextView) findViewById(R.id.txtView3);
        aEditText = (EditText) findViewById(R.id.aEditText);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setText("Submit"); //this is best practice.
        btnSubmit.setTextColor(Color.WHITE);
        view = this.getWindow().getDecorView();
        view.setBackgroundResource(R.color.teal);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String InputText;
                InputText = aEditText.getText().toString();

                txtView1.setVisibility(View.VISIBLE);
                txtView1.setText(InputText);
            }
            public void backgroundColor (View v) {
                view.setBackgroundResource(R.color.teal);
            }
        });
    }}

    /*
    public void ShowMeText(View view) {

        //First action happens when the button is clicked
        txtView1.setVisibility(View.VISIBLE);

        //Text is shown
        txtView1.setText(R.string.show_text);
    }
    */

